/* eslint-disable no-console */
const express = require('express');

const app = express();

const port = 5000;

app.get('/api/customers', (req, res) => {
  const customers = [
    { id: 1, name: 'Tony', surname: 'Stark' },
    { id: 2, name: 'Clark', surname: 'Kent' },
    { id: 3, name: 'Steve', surname: 'Rogers' },
  ];

  res.json(customers);
});

app.listen(port, () => console.log(`Server started on port ${port}`));
